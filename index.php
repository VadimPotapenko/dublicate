<?php
#=============================== setting ====================================#
include_once ('src/crest.php');
$logs = true;                                      // управление логированием
#============================================================================#
# $_REQUEST['data']['SOURCE_ID']
writeToLog($_REQUEST, $logs, 'Новое событие');
if ((isset($_REQUEST['ID']) || isset($_REQUEST['data']['ID'])) && (isset($_REQUEST['SOURCE_ID']) && $_REQUEST['SOURCE_ID'] == 15)) {
	$requestId = $_REQUEST['ID'] ? $_REQUEST['ID'] : $_REQUEST['data']['ID'];
	### ветка проверки дубликатов ###
	$lead = CRest::call('crm.lead.get', array('id' => $requestId));
	$phoneNum = $lead['result']['PHONE'][0]['VALUE'];
	writeToLog($lead, $logs, 'Лид');

	### ищем лиды и контакты с таким же номером тел ###
	$dubicateLead = CRest::call('crm.lead.list', array('filter' => array('!ID' => $requestId, 'PHONE' => $phoneNum)));
	$dublicateCompany = CRest::call('crm.company.list', array('filter' => array('PHONE' => $phoneNum)));
	writeToLog($dubicateLead, $logs, 'Лиды дубликаты');
	writeToLog($dublicateCompany, $logs, 'Компании дубликаты');

	if (!empty($dubicateLead['result'])) {
		foreach ($dubicateLead['result'] as $id) {
			$Ids['lead'] = array($id['ID']);
		}
	} else {
		$Ids['lead'] = 0;
	}
	
	if (!empty($dublicateCompany['result'])) {
		foreach ($dublicateCompany['result'] as $id) {
			$Ids['company'] = array($id['ID']);
		}
	} else {
		$Ids['company'] = 0;
	}

	### запуск бп ###
	$attempts = $_REQUEST['ATTEMPTS'] ? $_REQUEST['ATTEMPTS'] : 0;
	if ($Ids) {
		$bizProc = CRest::call('bizproc.workflow.start', array(
			'TEMPLATE_ID' => '73',
			'DOCUMENT_ID' => ['crm', 'CCrmDocumentLead', $requestId],
			'PARAMETERS'  => array(
				'leads_double'     => $Ids['lead'],
				'companies_double' => $Ids['company'],
				'attempts'         => $attempts
			)
		));
	}
	writeToLog($bizProc, $logs, 'Бизнес-процесс запущен');

} elseif (isset($_REQUEST['FROM_LINE']) && isset($_REQUEST['TO_NUMBER'])) {
	### ветка автодозвона ###
	$result = CRest::call('voximplant.callback.start', array(
		'FROM_LINE'         => $_REQUEST['FROM_LINE'],
		'TO_NUMBER'         => $_REQUEST['TO_NUMBER'],
		'TEXT_TO_PRONOUNCE' => 'Тест',
		'VOICE'             => 'ruinternalfemale'
	));
	writeToLog($result, $logs, 'Автодозвон');

	### обновляем сделку добавляя в пользовательское поле id звонка ###
	$deal = CRest::call('crm.lead.update', array('id' => $_REQUEST['LEAD_ID'], 'fields' => array('UF_CRM_1574152621' => $result['result']['CALL_ID'])));
	writeToLog($deal, $logs, 'Сделка обновлена');

} elseif (isset($_REQUEST['CALL_ID'])) {
	### ветка статистика звонков ###
	$static = CRest::call('voximplant.statistic.get', array('filter' => array('CALL_ID' => $_REQUEST['CALL_ID'])));
	$code = $static['result']['CALL_FAILED_CODE'];
	writeToLog($static, $logs, 'Статистика звонка');

	### БП ###
	$bizProc = CRest::call('bizproc.workflow.start', array(
		'TEMPLATE_ID' => '75',
		'DOCUMENT_ID' => ['crm', 'CCrmDocumentLead', $_REQUEST['LEAD_ID']],
		'PARAMETERS'  => array(
			'call_result' => $code
		)
	));
	writeToLog($bizProc, $logs, 'Бизнес-процесс ветки статистика звонков');

}

################################ functions ###################################
function writeToLog ($data, $bool, $title = 'DEBUG', $file = 'debug.txt') {
	if ($bool) {
		$log = "\n--------------------\n";
		$log .= date('d.m.Y H:i:s')."\n";
		$log .= $title."\n";
		$log .= print_r($data, 1);
		$log .= "\n--------------------\n";
		file_put_contents(__DIR__.'/'.$file, $log, FILE_APPEND);
	}
	return true;
}